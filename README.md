# Jira SDK TEST#

Console application to test connectivity with a JIRA server using the [Atlassian.NET SDK](https://bitbucket.org/farmas/atlassian.net-sdk).

Build and run the project, it will ask you for the url, user and password of the JIRA server. It will attempt to connect and download your favorite filters and the issue types.

Tracing is enabled, so you will see request and responses as part of the console output.